# **TILESERVER-GL**

TileServer GL is an open-source map server that serves raster and vector tiles based on Mapbox Vector Tile Specification. It's designed to be used with WebGL-powered, interactive maps and is optimized for fast and efficient serving of map data. TileServer GL supports both vector and raster tiles. Vector tiles contain vector data (like points, lines, and polygons) which can be styled dynamically in the browser. Raster tiles are pre-rendered images of map data.

## **Download file**

```wget https://github.com/maptiler/tileserver-gl/releases/download/v1.3.0/zurich_switzerland.mbtiles```

## **Installation**

```npm install -g tileserver-gl```

or run with docker image

```docker run --rm -it -v $(pwd):/data -p 8080:8080 maptiler/tileserver-gl```

Then open the browser and go to the link ```http://[server ip]:8080```

## **Configuration File**

Create a config.json file in the same directory in the following format.

```{
  "options": {
    "paths": {
      "root": "",
      "fonts": "fonts",
      "sprites": "sprites",
      "styles": "styles",
      "mbtiles": ""
    },
    "domains": [
      "localhost:8080",
      "127.0.0.1:8080"
    ],
    "formatQuality": {
      "jpeg": 80,
      "webp": 90
    },
    "maxScaleFactor": 3,
    "maxSize": 2048,
    "pbfAlias": "pbf",
    "serveAllFonts": false,
    "serveAllStyles": false,
    "serveStaticMaps": true,
    "tileMargin": 0
  },
  "styles": {
    "basic": {
      "style": "basic.json",
      "tilejson": {
        "type": "overlay",
        "bounds": [8.44806, 47.32023, 8.62537, 47.43468]
      }
    },
    "hybrid": {
      "style": "satellite-hybrid.json",
      "serve_rendered": false,
      "tilejson": {
        "format": "webp"
      }
    }
  },
  "data": {
    "zurich-vector": {
      "mbtiles": "zurich.mbtiles"
    }
  }
}
```
## **Style**
Style file in json format should be created inside the path mentioned in configuration file. A sample file provided below.

```{
  "version": 8,
  "name": "TEST",
  "metadata": {
    "openmaptiles:version": "3.x"
  },
  "sources": {
    "openmaptiles": {
      "type": "vector",
      "url": "mbtiles://name of input file"
    }
  },
  "glyphs": "{fontstack}/{range}.pbf",
  "layers": [
    {
      "id": "background",
      "type": "background",
      "paint": {
        "background-color": "rgb(239,239,239)"
      },
      "maxzoom": 24,
      "minzoom": 0
    },
    
    {
      "filter": [
        "all",
        [
          "==",
          "$type",
          "Polygon"
        ],
        [
          "in",
          "class",
          "residential",
          "suburb",
          "neighbourhood"
        ]
      ],
      "id": "landuse-residential",
      "paint": {
        "fill-color": "hsl(47, 13%, 86%)",
        "fill-opacity": 0.7
      },
      "source": "openmaptiles",
      "source-layer": "landuse",
      "type": "fill"
    },
    {
      "filter": [
        "==",
        "class",
        "grass"
      ],
      "id": "landcover_grass",
      "paint": {
        "fill-color": "hsl(82, 46%, 72%)",
        "fill-opacity": 0.45
      },
      "source": "openmaptiles",
      "source-layer": "landcover",
      "type": "fill"
    },
  ]
    "id": "Test"
}
```
sprites, fonts can also be added by creating respective files in the path mentioned in configuration file.


## **Cronjob**

Create a script
```
rm path/to/file/filename.osm.pbf
echo -ne '\n' | rm path/to/file/filename.mbtiles

mv path/to/file/filename.osm.pbf path/to/file/filename.osm.pbf
mv path/to/file/filename.mbtiles path/to/file/filename.mbtiles

set -e

wget https://link_to_download_file -O path/to/file/filename.osm.pbf

docker run -v path/to/file:/srv --rm tilemaker /srv/filename.osm.pbf --output=/srv/filename.mbtiles

docker kill -s HUP container_id
```
Create cron job with
```
* * * * * path/to/file/script >> path/to/log/cron.log 2>&1

```
Please see reference to setup cronjob


## **Integrating Tileserver in geonode**

```python
CUSTOM_TILE = [
  "url": "http://[server ip]:8080/styles/{id}/{z}/{x}/{y}.png"
  ]
```
## **OTHER REFERENCE**

https://github.com/maptiler/tileserver-gl

https://maptiler-tileserver.readthedocs.io/

https://tileserver.readthedocs.io/en/latest/installation.html

https://www.geeksforgeeks.org/crontab-in-linux-with-examples/
